import { Component } from '@angular/core';


import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cameraData: string;
  photoTaken: boolean;
  cameraUrl: string;
  photoSelected: boolean;

  constructor(public navCtrl: NavController, private camera: Camera,
  private _DomSanitizationService: DomSanitizer) {
    this.photoTaken = false;
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }


    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI

      // If it's base64:
      this.cameraData = 'data:image/jpeg;base64,' + imageData;

      this.cameraUrl = imageData;
      this.photoSelected = false;
      this.photoTaken = true;




    }, (err) => {
      // Handle error
      alert('No Camera or error');
    });
  }


    selectFromGallery() {
      var options = {
        sourceType:  this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType:  this.camera.DestinationType.FILE_URI
      };
       this.camera.getPicture(options).then((imageData) => {
        this.cameraUrl = imageData;
        this.photoSelected = true;
        this.photoTaken = false;
      }, (err) => {
        // Handle error
        alert(err);
      });
    }
 
  /*
   openCamera() {
     var options = {
       sourceType: Camera.PictureSourceType.CAMERA,
       destinationType: Camera.DestinationType.DATA_URL
     };
     Camera.getPicture(options).then((imageData) => {
       this.cameraData = 'data:image/jpeg;base64,' + imageData;
       this.photoTaken = true;
       this.photoSelected = false;
     }, (err) => {
      alert(err);
     });
   }
 */
}
